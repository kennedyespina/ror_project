class UploadedFilesController < ApplicationController
	def index
		@uploaded_files = UploadedFile.all
	end

	def show
		@uploaded_file = UploadedFile.find(params[:id])
	end

	def new
		@uploaded_file = UploadedFile.new
	end

	def create
		@uploaded_file = UploadedFile.new(uploaded_file_params)
		if @uploaded_file.save
			redirect_to uploaded_files_path
		else
			render :new
		end
	end

	def edit
		@uploaded_file = UploadedFile.find(params[:id])
	end

	def update
		@uploaded_file = UploadedFile.find(params[:id])

		if @uploaded_file.update_attributes(uploaded_file_params)
			redirect_to uploaded_files_path
		else
			render :edit
		end
	end

	def destroy
		@uploaded_file = UploadedFile.find(params[:id])
		@uploaded_file.destroy
		redirect_to uploaded_files_path
	end

	def uploaded_file_params
		params.require(:uploaded_file).permit!
	end
end
